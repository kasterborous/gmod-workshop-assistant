#! /bin/bash

progname=$(basename $0)
line="______________________________________________"

supported_options=("pack" "update" "create" "icon" "help" "clean" "link" "--help" "-h" "-?")

ERROR_BAD_SUBCOMMAND=1
ERROR_TOO_FEW_ARGS=2
ERROR_MISSING_FOLDER=3
ERROR_TOO_FEW_ARGS=4
ERROR_MISSING_COMMAND=5
ERROR_ADDON_NOT_FOUND=6

print_usage_and_exit() {
	echo "Usage:"
	echo "  $progname <subcommand> <folder>"
	echo $line
	echo "Subcommands:"
	echo "  pack     - pack the addon into .gma file"
	echo "  update   - upload the addon to steam"
	echo "  create   - upload the addon to steam"
	echo "  icon     - change the addon's icon"
	echo "  clean    - clear all .gma files in the folder"
	echo "  link     - print the steam link to the addon"
	echo $line
	echo "Note:"
	echo "  This tool requires addon.json in the folder:"
	echo "  For \"update\", it must contain \"workshop_id\";"
	echo "  For \"create\" and \"icon\": \"icon_path\"."
	echo $line

	exit $1
}

print_help_and_exit() {
	echo -e "\nHelp for $progname\n$line"
	print_usage_and_exit $1
}

print_error() { echo -e "\nERROR: $1\n" >&2; }
print_error_and_exit() { print_error "$1" && exit $2; }
print_error_usage_and_exit() { print_error "$1" && print_usage_and_exit $2; }

check_folder() {
	[[ ! -d $1 ]] && \
		print_error_and_exit "Failed to open folder $folder" $ERROR_MISSING_FOLDER
}

check_file() {
	unset hint_text
	[[ -z $2 ]] || hint_text="\nHINT: $2"
	[[ ! -f $1 ]] && \
		print_error_and_exit "Failed to open file $1$hint_text" $ERROR_MISSING_FILE
}

check_command() {
	[[ $(command -v $1) = "" ]] && \
		print_error_and_exit "Missing command '$1'" $ERROR_MISSING_COMMAND
}

json_read() {
	check_command python
	file=$1
	param=$2
	cat $file | \
		python -c "import sys, json; addon=json.load(sys.stdin); print(addon['$param'] if ('$param' in addon) else \"\")"
}

get_workshop_id() {
	json_file=$1
	workshop_id=$(json_read $json_file "workshop_id")

	[[ $workshop_id = "" ]] && \
		print_error_and_exit \
			"File $json_file has no \"workshop_id\" entry." $ERROR_ADDON_NOT_FOUND
}

get_icon_path() {
	json_file=$1
	icon_path=$(json_read $json_file "icon_path")

	[[ $icon_path = "" ]] && \
			print_error_and_exit \
				"File $json_file has no \"icon_path\" entry." $ERROR_ADDON_NOT_FOUND

}

ask() {
	echo -e -n "\n$1 [Y/N] "
	read ans
	[[ $ans = "y" || $ans = "yes" || $ans = "Y" ]] || exit 1
	echo
}

verbose_run_command() {
	command="$*"
	echo -e "$command\n"
	eval $command
	return $?
}

ask_run_command() {
	command="$*"
	ask "Will run command: $command\nAll OK?"
	eval $command
	return $?
}

ask_rm_file() {
	file=$1
	[[ ! -f $file ]] && return
	ask "Should file $file be removed?"
	rm -vf $file
}

check_addon() {
	workshop_id=$1

	found_addon=$(gmpublish list | grep $workshop_id)
	[[ $found_addon = "" ]] && \
		print_error_and_exit \
			"Failed to update the addon with id $workshop_id" $ERROR_ADDON_NOT_FOUND

	ask "\nUpdating addon:\n$found_addon\n\nContinue?"
}



################################################################################

[[ $# < 1 ]] && \
	print_error_usage_and_exit "Too few arguments! ($#)" $ERROR_TOO_FEW_ARGS

subcommand=$1
shift

[[ $subcommand = "--help" || $subcommand = "help" ]] && print_help_and_exit 0
[[ $subcommand = "-h" || $subcommand = "-?" ]] && print_help_and_exit 0

[[ ! " ${supported_options[*]} " =~ " $subcommand " ]] && \
	print_error_usage_and_exit "Unsupported subcommand: $subcommand" $ERROR_BAD_SUBCOMMAND


folder=$1
[[ -z $folder ]] && folder="."
check_folder $folder
json_file=$folder/addon.json
gma_file=$folder/addon.gma
shift

if [[ $subcommand = "pack" ]]
then
	check_command gmad
	ask_rm_file $gma_file
	ask_run_command "gmad create -folder $folder -out $gma_file"
	exit $?
fi

if [[ $subcommand = "update" ]]
then
	changelog="$*"

	check_command gmpublish
	check_file $gma_file "To pack the addon, use:\n    $0 pack $folder"
	check_file $json_file

	unset workshop_id
	get_workshop_id $json_file
	check_addon $workshop_id

	[[ -z $changelog ]] && \
		echo -e "\nWARNING: Changelog not specified!\n"

	ask_run_command "gmpublish update -id $workshop_id -addon $gma_file -changes \"$changelog\""
	success=$?
	[[ $success = 0 ]] && ask_rm_file $gma_file
	exit $success
fi

if [[ $subcommand = "icon" ]]
then
	check_command gmpublish
	check_file $json_file

	unset icon_path
	get_icon_path $json_file
	icon_file=$folder/$icon_path
	check_file $icon_file

	unset workshop_id
	get_workshop_id $json_file
	check_addon $workshop_id

	ask_run_command "gmpublish update -id $workshop_id -icon $icon_file"
	exit $?
fi

if [[ $subcommand = "create" ]]
then
	check_command gmpublish
	check_file $gma_file
	check_file $json_file

	unset icon_path
	get_icon_path $json_file
	icon_file=$folder/$icon_path
	check_file $icon_file

	addon_name=$(json_read $json_file "title")

	ask "Creating a NEW addon \"$addon_name\"\nContinue?"

	ask_run_command "gmpublish.exe create -icon $icon_file -addon $gma_file"
	success=$?
	[[ $success = 0 ]] && ask_rm_file $gma_file
	exit $success
fi

if [[ $subcommand = "clean" ]]
then
	rm -vf $folder/*.gma
fi

if [[ $subcommand = "link" ]]
then
	check_file $json_file
	unset workshop_id
	get_workshop_id $json_file
	echo "https://steamcommunity.com/sharedfiles/filedetails/?id=$workshop_id"
fi