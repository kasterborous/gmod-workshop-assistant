
GWA / gmod workshop assistant
---
Installation (git bash):
1. Download the folder with the assistant on your hard drive
2. Open or create file `C:/Users/<user>/.bashrc`
3. Add the following lines to the file:
```bash
export PATH="<steam_library>/steamapps/common/GarrysMod/bin:$PATH"
export PATH="<path_to_this_folder>:$PATH"
```
NOTE: for git bash, replace `C:` with `/c/` in the folder paths

Example:  
if `gmod-workshop-assistant` is put in the `addons` folder of gmod, use:
```bash
export PATH="/c/Program Files (x86)/Steam/steamapps/common/GarrysMod/bin:$PATH"
export PATH="/c/Program Files (x86)/Steam/steamapps/common/GarrysMod/garrysmod/addons/gmod-workshop-assistant:$PATH"
```

4. Open `Git Bash` and type in `gwa help`


---
